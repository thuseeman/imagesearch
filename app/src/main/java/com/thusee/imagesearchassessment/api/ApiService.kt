package com.thusee.imagesearchassessment.api

import com.thusee.imagesearchassessment.BuildConfig
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.QueryMap

/**
 * Created by rthusee on 2019-12-01
 */
interface ApiService {

    @GET("Search/ImageSearchAPI")
    fun searchImages(@HeaderMap headers: Map<String, String>, @QueryMap queries: Map<String, String>): Flowable<Response<ImageSearchResponse>>

    companion object{
        fun createApiService(): ApiService{
            return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiService::class.java)
        }
    }
}