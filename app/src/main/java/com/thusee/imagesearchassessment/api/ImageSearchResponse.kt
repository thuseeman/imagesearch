package com.thusee.imagesearchassessment.api

import com.thusee.imagesearchassessment.ui.search.ImageSearch

/**
 * Created by rthusee on 2019-12-01
 */
data class ImageSearchResponse (val totalCount: Int = 0,
                                val value: MutableList<ImageSearch>? = null)