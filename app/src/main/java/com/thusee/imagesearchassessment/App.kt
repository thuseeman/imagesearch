package com.thusee.imagesearchassessment

import android.app.Application
import com.thusee.imagesearchassessment.di.appMoudles
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Created by rthusee on 2019-12-01
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            // declare used Android context
            androidContext(this@App)

            modules(appMoudles)
        }
    }
}