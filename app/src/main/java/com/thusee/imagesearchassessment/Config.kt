package com.thusee.imagesearchassessment

/**
 * Created by rthusee on 2019-12-01
 */

val GRID_SIZE = 2

val API_HEADERS = mapOf(
    "x-rapidapi-host" to "contextualwebsearch-websearch-v1.p.rapidapi.com",
    "x-rapidapi-key" to "8d5bd485cdmsh70fac8445abdde2p15b763jsn5d195a1b6a6d"
)