package com.thusee.imagesearchassessment.ui.web

import android.app.AlertDialog
import android.content.Context
import android.net.http.SslError
import android.os.Build
import android.webkit.*
import timber.log.Timber

/**
 * Created by rthusee on 2019-12-01
 */
class ImageWebViewClient (private var context: Context) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view?.loadUrl(request?.url.toString())

            return true
        }
        return super.shouldOverrideUrlLoading(view, request)
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        view.loadUrl(url)
        return true
//        return super.shouldOverrideUrlLoading(view, url)
    }

    override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
        val builder = AlertDialog.Builder(context)
        var message = "SSL Certificate error."
        when (error.primaryError) {
            SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
            SslError.SSL_EXPIRED -> message = "The certificate has expired."
            SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
            SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
        }
        message += " Do you want to continue anyway?"

        builder.setTitle("SSL Certificate Error")
        builder.setMessage(message)
        builder.setPositiveButton(
            "continue"
        ) { dialog, which -> handler.proceed() }
        builder.setNegativeButton(
            "cancel"
        ) { dialog, which -> handler.cancel() }
        val dialog = builder.create()
        dialog.show()

        return super.onReceivedSslError(view, handler, error)
    }

    override fun onReceivedError(
        view: WebView,
        request: WebResourceRequest,
        error: WebResourceError
    ) {
        Timber.d( "Got Error! $error")
    }

    override fun onPageFinished(view: WebView, url: String) {
        super.onPageFinished(view, url)
    }

    private fun openBrowser(url: String) {
        val url: String = url
    }
}