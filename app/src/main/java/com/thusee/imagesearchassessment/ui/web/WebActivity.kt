package com.thusee.imagesearchassessment.ui.web

import android.os.Bundle
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_BACK
import com.thusee.imagesearchassessment.R
import com.thusee.imagesearchassessment.ui.base.BaseActivity
import kotlinx.android.synthetic.main.webview_activity.*

/**
 * Created by rthusee on 2019-12-01
 */
class WebActivity : BaseActivity() {

    companion object {
        const val SEARCH_TEXT = "searchText"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview_activity)

        var url = "https://www.google.com"
        val text = intent.getStringExtra(SEARCH_TEXT) ?: url
        if (text.startsWith("http")) {
            //this is an url
            url = text
        } else if (text.startsWith("www") || text.endsWith(".com")) {
            url = "https://$text"
        } else {
            url = "https://www.google.com/search?q=$text"
        }

        // enable java script
        web.settings.javaScriptEnabled = true
        web.webViewClient = ImageWebViewClient(this)
        web.loadUrl(url)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                //webview go back to the previous page
                KEYCODE_BACK -> {
                    if (web.canGoBack()) {
                        web.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

}