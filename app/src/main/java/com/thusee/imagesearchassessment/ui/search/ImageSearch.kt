package com.thusee.imagesearchassessment.ui.search

import androidx.room.Entity

/**
 * Created by rthusee on 2019-12-01
 */
@Entity(primaryKeys = ["url"])
data class ImageSearch (
    val url: String,
    val thumbnail: String,
    val height: Int?,
    val width: Int?,
    val thumbnailHeight: Int?,
    val thumbnailWidth: Int?,
    var search: String?
)