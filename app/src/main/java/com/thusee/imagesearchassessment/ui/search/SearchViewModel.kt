package com.thusee.imagesearchassessment.ui.search

import com.thusee.imagesearchassessment.API_HEADERS
import com.thusee.imagesearchassessment.api.ApiService
import com.thusee.imagesearchassessment.api.ImageSearchResponse
import com.thusee.imagesearchassessment.db.ImageFinderDb
import io.reactivex.Flowable
import retrofit2.Response
import timber.log.Timber

/**
 * Created by rthusee on 2019-12-01
 */
class SearchViewModel (private val apiService: ApiService, private val db: ImageFinderDb) {
    private val searchResultList = mutableListOf<ImageSearch>()
    private var pageNo: Int = 1

    fun searchFromNetwork(searchText: String): Flowable<Response<ImageSearchResponse>> {
        Timber.d("searching: $searchText, pageNo:$pageNo")
        val queires = mapOf(
            "autoCorrect" to "true",
            "pageNumber" to "$pageNo",
            "pageSize" to "10",
            "q" to searchText,
            "safeSearch" to "false"
        )
        return apiService.searchImages(API_HEADERS, queires).doOnNext {
            if (it.isSuccessful) {
                it.body()?.let { result ->
                    if (result.value != null) {
                        setResult(result.value)
                        Timber.d("resultCount:%s", searchResultList.size)
                        saveToDatabase(result.value, searchText)
                    }
                }
            }
        }
    }

    fun searchFromLocal(searchText: String) =
        db.imageSearchDao().searchImages(searchText).doOnNext {
            setResult(it)
        }

    fun saveToDatabase(list: MutableList<ImageSearch>, searchText: String) {
        list.forEach { it.search = searchText }
        db.imageSearchDao().insertAll(list)
    }

    fun setResult(results: MutableList<ImageSearch>) {
        results.forEach { image ->
            if (searchResultList.none { it.url == image.url }) {
                searchResultList.add(image)
            }

        }
    }

    fun getSearchResultList() = searchResultList

    fun increasePageNo() {
        pageNo++
    }

    fun reset() {
        Timber.d("reset")
        pageNo = 1
        searchResultList.clear()
    }
}