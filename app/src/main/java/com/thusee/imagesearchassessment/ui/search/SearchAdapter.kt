package com.thusee.imagesearchassessment.ui.search

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.thusee.imagesearchassessment.GRID_SIZE
import com.thusee.imagesearchassessment.R

/**
 * Created by rthusee on 2019-12-01
 */
class SearchAdapter (private val searchResultList: List<ImageSearch>) :
    RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    var size: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        size = parent.context.resources.displayMetrics.widthPixels / GRID_SIZE
        return SearchViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.search_result_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = searchResultList.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val imageSearch = searchResultList[position]

        Glide.with(holder.imageView)
            .load(imageSearch.thumbnail)
            .placeholder(getProgress(holder.itemView.context))
            .error(R.drawable.ic_image_black)
            .centerCrop()
            .into(holder.imageView)
        holder.imageView.layoutParams.height = size

        holder.itemView.setOnClickListener {
            popUpImage(it.context, imageSearch.url)
        }
    }

    fun popUpImage(context: Context, imageUrl: String) {
        val builder = Dialog(context)
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE)
        builder.window?.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT)
        )
        val imageView = ImageView(context)
        val widthPixels = context.resources.displayMetrics.widthPixels
        imageView.layoutParams = ViewGroup.LayoutParams(
            widthPixels,
            widthPixels
        )

        Glide.with(imageView)
            .load(imageUrl)
            .placeholder(R.drawable.ic_image_white)
            .error(R.drawable.ic_error_accent)
            .fitCenter()
            .into(imageView)

        builder.addContentView(
            imageView, RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        builder.show()
    }

    private fun getProgress(context: Context): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth =
            context.resources.getDimensionPixelSize(R.dimen.stroke_width).toFloat()
        circularProgressDrawable.centerRadius =
            context.resources.getDimensionPixelSize(R.dimen.center_radius).toFloat()

        circularProgressDrawable.start()
        return circularProgressDrawable
    }

    class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.image)
    }
}

