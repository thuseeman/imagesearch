package com.thusee.imagesearchassessment.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thusee.imagesearchassessment.GRID_SIZE
import com.thusee.imagesearchassessment.R
import com.thusee.imagesearchassessment.ui.base.BaseActivity
import com.thusee.imagesearchassessment.ui.web.WebActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.search_activity.*
import org.koin.android.ext.android.inject

/**
 * Created by rthusee on 2019-12-01
 */
class SearchActivity : BaseActivity(){

    val searchViewModel: SearchViewModel by inject()
    private val disposable: CompositeDisposable by inject()

    lateinit var adapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_activity)

        adapter = SearchAdapter(searchViewModel.getSearchResultList())
        searchResultList.layoutManager = GridLayoutManager(this, GRID_SIZE)
        searchResultList.adapter = adapter

//        input.setText("dog")

        input.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                dismissKeyboard(view.windowToken)
                doSearch(true)
                true
            }
            false
        }

        swipeRefresh.setOnRefreshListener {
            doSearch(true)
        }

        searchResultList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition == adapter.itemCount - 1) {
                    if (!swipeRefresh.isRefreshing) {//check loading
                        searchViewModel.increasePageNo()
                        doSearch(false)
                    }
                }
            }
        })

        web.setOnClickListener {
            //open web view
            if (validate()) {
                //input.text.toString()
                val intent = Intent(this@SearchActivity, WebActivity::class.java)
                intent.putExtra(WebActivity.SEARCH_TEXT, input.text.toString())
                startActivity(intent)
            }
        }
    }

    private fun doSearch(new: Boolean) {
        if(new){
            searchViewModel.reset()
            updateUi()
        }
        if (validate()) {
            disposable.add(
                searchViewModel.searchFromLocal(input.text.toString()).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { swipeRefresh.isRefreshing = true }
//                    .doOnTerminate { swipeRefresh.isRefreshing = false }
                    .subscribe({ updateUi() }, { t -> t.printStackTrace() })
            )

            disposable.add(
                searchViewModel.searchFromNetwork(input.text.toString()).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnSubscribe { swipeRefresh.isRefreshing = true }
                    .doOnTerminate { swipeRefresh.isRefreshing = false }
                    .subscribe({ updateUi() }, { t -> t.printStackTrace() })
            )
        }
    }

    private fun updateUi() {
        searchResultList.adapter?.notifyDataSetChanged()
    }

    private fun validate(): Boolean {
        val searchText = input.text.toString()
        if (searchText.isEmpty()) {
            input.error = getString(R.string.required)
            swipeRefresh.isRefreshing = false
            return false
        }
        input.error = null
        return true
    }

    override fun onResume() {
        super.onResume()
        input.text.clear()
    }

}