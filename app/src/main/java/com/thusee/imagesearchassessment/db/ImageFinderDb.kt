package com.thusee.imagesearchassessment.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.thusee.imagesearchassessment.ui.search.ImageSearch

/**
 * Created by rthusee on 2019-12-01
 */

@Database(entities = [ImageSearch::class], version = 1)
abstract class ImageFinderDb : RoomDatabase() {
    abstract fun imageSearchDao(): ImageSearchDao

    companion object {

        @Volatile private var INSTANCE: ImageFinderDb? = null

        fun getInstance(context: Context): ImageFinderDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                ImageFinderDb::class.java, "ImageFinder.db")
                .build()
    }
}