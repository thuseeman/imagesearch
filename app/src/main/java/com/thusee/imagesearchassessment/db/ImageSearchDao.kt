package com.thusee.imagesearchassessment.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thusee.imagesearchassessment.ui.search.ImageSearch
import io.reactivex.Flowable

/**
 * Created by rthusee on 2019-12-01
 */

@Dao
interface ImageSearchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(imageSearch: ImageSearch)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(imageSearchList: List<ImageSearch>)

    @Query("SELECT * FROM ImageSearch WHERE search = :searchText")
    fun searchImages(searchText: String): Flowable<MutableList<ImageSearch>>
}