package com.thusee.imagesearchassessment.di

import com.thusee.imagesearchassessment.api.ApiService
import com.thusee.imagesearchassessment.db.ImageFinderDb
import com.thusee.imagesearchassessment.ui.search.SearchViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.dsl.module

/**
 * Created by rthusee on 2019-12-01
 */

val appMoudles = module {
    factory { SearchViewModel(get(), get()) }
    single {
        ApiService.createApiService()
    }
    single {
        ImageFinderDb.getInstance(get())
    }
    factory { CompositeDisposable() }
}